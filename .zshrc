# Set the directories where we want to store downloadable stuff.
ZINIT_HOME="${XDG_DATA_HOME:-${HOME}/.local/share}/zinit/zinit.git"
OH_MY_POSH_HOME="${HOME}/Applications"

# Download and install Zinit if it's not there yet.
if [ ! -d "$ZINIT_HOME" ]; then
	mkdir -p "$(dirname $ZINIT_HOME)"
	git clone https://github.com/zdharma-continuum/zinit.git "$ZINIT_HOME"
fi

# Download and install Oh-my-posh if it's not there yet.
if [ ! -f "${OH_MY_POSH_HOME}/oh-my-posh" ]; then
	curl -s https://ohmyposh.dev/install.sh | bash -s -- -d ${OH_MY_POSH_HOME}
fi

# Source/load Zinit.
source "${ZINIT_HOME}/zinit.zsh"

# Add in Zsh plugins.
zinit light zsh-users/zsh-syntax-highlighting
zinit light zsh-users/zsh-completions
zinit light zsh-users/zsh-autosuggestions
zinit light Aloxaf/fzf-tab

# Load completions.
autoload -U compinit && compinit
zinit cdreplay -q

# Useful aliases.
alias dir='eza --long --header --git --classify --icons --all --time-style=long-iso --group-directories-first'
alias ls='ls --color'
alias tick='tock --second --color 4 --center --military --width 4 --height 2 --format ""'

# Aditional movement keys.
bindkey "^[[1~" beginning-of-line      # Home
bindkey "^[[4~" end-of-line            # End
bindkey "^[[3~" delete-char            # Del
bindkey "^[[1;3C" forward-word         # Alt + Right Arrow
bindkey "^[[1;3D" backward-word        # Alt + Left Arrow
bindkey "^H" backward-kill-word        # Ctrl + Backspace
bindkey '^p' history-search-backward
bindkey '^n' history-search-forward

# History configuration.
HISTSIZE=5000
SAVEHIST=$HISTSIZE
HISTFILE=~/.zsh_history
HISTDUP=erase
setopt appendhistory
setopt sharehistory
setopt hist_ignore_space
setopt hist_ignore_all_dups
setopt hist_save_no_dups
setopt hist_ignore_dups
setopt hist_find_no_dups

# Completion styling.
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
zstyle ':completion:*' menu no
zstyle ':fzf-tab:complete:cd:*' fzf-preview 'exa --git --classify --icons --all --group-directories-first $realpath'
zstyle ':fzf-tab:complete:__zoxide_z:*' fzf-preview 'exa --header --git --classify --icons --all --group-directories-first $realpath'

# Path building.
export PATH=$PATH:$HOME/Applications:$HOME/.cargo/bin

# Shell integrations.
eval "$(fzf --zsh)"
eval "$(zoxide init --cmd cd zsh)"
eval "$(oh-my-posh init zsh --config $HOME/.config/oh-my-posh/zen.toml)"

# Tmux initialization.
if command -v tmux &> /dev/null && [ -n "$PS1" ] && [[ ! "$TERM" =~ screen ]] && [[ ! "$TERM" =~ tmux ]] && [ -z "$TMUX" ]; then
  exec tmux
fi

# Avoid the 30 seconds screen switch-off in my laptop.
# https://askubuntu.com/a/1426429
xset s off && xset -dpms
