require "nvchad.options"

vim.api.nvim_create_autocmd("VimLeave", {
  pattern = "*",
  callback = function()
    vim.opt.guicursor = "a:ver25"
  end,
})

vim.opt.colorcolumn = "80"
vim.o.cursorlineopt = "both"
vim.o.expandtab = true
vim.o.shiftwidth = 4
vim.o.tabstop = 4
vim.o.softtabstop = 4
vim.o.smartindent = true
